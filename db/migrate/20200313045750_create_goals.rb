class CreateGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :goals do |t|
      t.string :name
      t.datetime :start_time
      t.datetime :end_time
      t.text :description
      t.integer :status
      t.integer :author_id
      t.string :project_id
      t.timestamps
    end
  end
end
