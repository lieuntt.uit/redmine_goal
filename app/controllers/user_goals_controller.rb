class UserGoalsController < ApplicationController
  before_action :require_login
  before_action :current_user

  def index
    @goals = Goal.where(author: current_user)
  end

  def new
    @goal = Goal.new(author: current_user)
  end

  def create
    @goal = Goal.new(author: current_user)
    @goal.assign_attributes(goal_params)

    if @goal.valid?
      @goal.save
      redirect_to user_goals_path, notice: 'Successful create.'
    else
      render :new
    end  
  end

  def show
    @goal = Goal.find(params[:id])
  end

  def update
    @goal = Goal.find(params[:id])
    @goal.assign_attributes(goal_params)

    if @goal.valid?
      @goal.save
      redirect_to user_goal_path(@goal), notice: 'Successful update.'
    else
      render :edit
    end  
  end

  def edit
    @goal = Goal.find(params[:id])
  end

  private

  def current_user
    @user = User.current
  end

  def goal_params
    params.require(:goal).permit!
  end
end
