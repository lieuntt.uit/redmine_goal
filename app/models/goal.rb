class Goal < ActiveRecord::Base
  belongs_to :project, foreign_key: 'project_id', primary_key: 'identifier'
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'

  validates :author, :start_time, :end_time, :name, :description, presence: true

  enum status: { open: 0, doing: 1, pending: 2, done: 3, close: 4 }

  def percent_of_doing_time
    current_time_value = (Time.current.to_date - start_time.to_date).to_i + 1
    end_time_value = (end_time.to_date - start_time.to_date).to_i + 1
    percent = (current_time_value.to_f / end_time_value.to_f) * 100
    percent.to_i
  end
end
