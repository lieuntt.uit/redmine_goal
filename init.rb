require 'redmine'

Redmine::Plugin.register :redmine_goal do
  name 'Redmine Goal plugin'
  author 'Lieu Nguyen'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  require_dependency 'polls_hook_listener'
  require_dependency 'redmine_goal/goal'

  project_module :project_goals do
    permission :view_goals, goals: :index
    permission :new_goals, goals: :new
    permission :create_goals, goals: :create
    permission :view_goal, goals: :show
    permission :edit_goal, goals: :edit
    permission :update_goal, goals: :update
  end

  menu :project_menu, :goals, { controller: :goals, action: :index }, caption: 'Project goals', after: :settings, param: :project_id
  menu :application_menu, :user_goals, { controller: 'user_goals', action: 'index' }, caption: 'My goals'
end
