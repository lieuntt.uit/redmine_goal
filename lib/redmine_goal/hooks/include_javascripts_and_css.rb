module DemoGoal
  module Hooks
    class IncludeJavascriptsAndCss < Redmine::Hook::ViewListener
      include ActionView::Helpers::TagHelper

      def view_layouts_base_html_head(context = {})
        javascript_include_tag(:chart, :plugin => 'demo_goal') +
          stylesheet_link_tag(:chart, :plugin => 'demo_goal')
      end
    end
  end
end
