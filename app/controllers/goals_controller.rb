class GoalsController < ApplicationController
  before_action :find_project
  before_action :authorize

  def index
    @goals = Goal.where(project: @project)
  end

  def new
    @goal = Goal.new
  end

  def show
    @goal = Goal.find(params[:id])
  end

  def edit
    @goal = Goal.find(params[:id])
  end

  def update
    @goal = Goal.find(params[:id])
    @goal.assign_attributes(params[:goal].permit!)
    if @goal.valid?
      @goal.save
      redirect_to goal_path(@goal, project_id: @goal.project_id), notice: 'Successful update.'
    else
      render :edit
    end
  end

  def create
    @goal = Goal.new(author: User.current)
    @goal.assign_attributes(params[:goal].permit!)
    if @goal.valid?
      @goal.save
      redirect_to goals_path(project_id: params[:project_id]), notice: 'Successful create.'
    else
      render :new
    end
  end

  private

  def find_project
  	@project = Project.find(params[:project_id])
  end
end
