class PollsHookListener < Redmine::Hook::ViewListener
	def view_projects_show_left(context = {})
		context[:goals_size] = Goal.all.size
		context[:controller].send(:render_to_string, {
			partial:'goals/goals_overview',
			locals: context
		})
	end
  
  def view_projects_show_right(context = {})
    return content_tag("p", "Custom content added to the right")
  end
end